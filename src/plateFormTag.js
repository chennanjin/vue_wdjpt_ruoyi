// 根路径地址
export const BASE_URL = {
  url: "http://120.79.190.94:8091",
  lookThroughPicurl: "http://120.76.156.85:9000",
};

export const DataTag = {
  plateName: {
    name: "思政资源库平台",
  },
  VisURL: {
    // 访问的目标路径
    path: "http://8.134.163.242:8091",
  },
  actionsUrl: {
    name: "单个上传图片的地址",
    url: BASE_URL.url + "/szpt/file/uploadImage",
  },
  picsActionUrl: {
    name: "图片批量上传地址",
    url: BASE_URL.url + "/file/uploadImageList",
  },
};
