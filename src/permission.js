import router from "./router";
import store from "./store";
import { Message } from "element-ui";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "@/utils/auth";
import { isRelogin } from "@/utils/request";

NProgress.configure({ showSpinner: false });

const whiteList = ["/login", "/register"];

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (getToken()) {
    to.meta.title && store.dispatch("settings/setTitle", to.meta.title);
    /* has token*/
    if (to.path === "/login") {
      //在有token的情况下还要跳转登录页面的话，会重定向到根路径，即不能重复登录
      next({ path: "" });
      NProgress.done();
    } else {
      //查看是否有角色权限信息，如果有需要先检查路由权限是否满足
      if (store.getters.roles.length === 0) {
        isRelogin.show = true;
        // 判断当前用户是否已拉取完user_info信息
        store
          .dispatch("GetInfo")
          .then(() => {
            isRelogin.show = false;
            store.dispatch("GenerateRoutes").then((accessRoutes) => {
              // 根据roles权限生成可访问的路由表
              router.addRoutes(accessRoutes); // 动态添加可访问路由表
              next({ ...to, replace: true }); // hack方法 确保addRoutes已完成
            });
          })
          .catch((err) => {
            store.dispatch("LogOut").then(() => {
              Message.error(err);
              next({ path: "/login" });
            });
          });
      } else {
        //有角色权限信息的话直接跳转即可
        next();
      }
    }
  } else if (to.meta.requireAuth == false) {
    // 当设置不需要登录直接就能访问
    if (getToken()) {
      // 如果用户已经登录了，就让他直接访问
      next();
    } else if (to.path == "/szptLogin/login") {
      // 如果用户没有登录的且访问的路径是登录路径就让用户访问
      next();
    } else if(to.path == "/szptLogin/register")
      {
        next()
      }    else {
      // 没有登录也且不是去登录页面的话就全部重定向到登录页
      next(`/szptLogin/login?redirect=${to.fullPath}`);
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      //在白名单的都是不需要登录状态的路由比如登录页面，直接跳转即可
      // 在免登录白名单，直接进入
      next();
    } else {
      next(`/login?redirect=${to.fullPath}`); // 否则全部重定向到登录页
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  NProgress.done();
});
