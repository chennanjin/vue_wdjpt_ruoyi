import request from "@/utils/request.js";

// 登录查询
export function login(data) {
  return request({
    url: "/login",
    method: "post",
    data: data,
  });
}

// 用户注册
export function register(data) {
  return request({
    url: "/szpt/register",
    method: "post",
    data: data,
  });
}

// 用户注销登录
export function logout() {
  return request({
    url: "/logout",
    method: "post",
  });
}
