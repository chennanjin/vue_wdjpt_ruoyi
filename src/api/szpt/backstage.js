import request from "@/utils/request.js";
// import { getToken } from "@/utils/auth.js";

// 获取后台帖子列表
export function getPostListBackstage(data) {
  return request({
    url: `/szpt/post/bgList`,
    params: {
      userId: data.userId,
      tagIds: data.tagIds,
      pageNum: data.pageNum,
      pageSize: data.pageSize,
      sortType: data.sortType,
      keyWord: data.keyWord,
      state: data.state,
      status: data.status,
    },
    method: "get",
  });
}
// 帖子审核
export function changeStateStatus(data) {
  let fmtData = {
    state: data.state,
    status: data.status,
    id: data.id,
  };
  // 创建 FormData 对象
  let formData = new FormData();
  // 遍历对象的属性并添加到 FormData 中
  for (let key in fmtData) {
    if (fmtData.hasOwnProperty(key)) {
      formData.append(key, fmtData[key]);
    }
  }
  // 返回请求，使用 FormData 作为 data 参数
  return request.post("/szpt/post/state", formData, {
    // 设置请求头，告诉服务器发送的是 multipart/form-data
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
