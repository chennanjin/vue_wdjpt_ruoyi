import request from "@/utils/request.js";
// 检索帖子
export function searchDataByKeyword(pageNum, pageSize, keyWord) {
  return request({
    url: `szpt/post/listByWord?pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC&keyWord=${keyWord}`,
    method: "get",
  });
}
// 检索用户数据
export function searchUserByKeyword(pageNum, pageSize, keyWord) {
  return request({
    url: `/szpt/user/keyWordList?pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC&keyWord=${keyWord}`,
    method: "get",
  });
}
