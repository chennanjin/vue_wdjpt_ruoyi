import request from "@/utils/request.js";

export function getTeacherInfo() {
    let tokenStr = window.localStorage.getItem("tokenStr");
    return request({
        url: "szpt/teachermsg/getlist",
        method: "get",
        headers: {
            Authorization: tokenStr,
        },
    });
}
