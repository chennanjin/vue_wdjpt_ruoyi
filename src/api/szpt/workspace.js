// 这里是保存帖子的接口
import request from "@/utils/request.js";
// 用户量和今日活跃量
export function getUserStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=userStatisticsStrategy",
    method: "get",
  });
}
// 帖子数据量统计
export function getPostStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=postStatisticsStrategy",
    method: "get",
  });
}
// 新增用户数据
export function getNewUserStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=newUserStatisticsStrategy",
    method: "get",
  });
}
// 举报指标
export function getReportStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=reportStatisticsStrategy",
    method: "get",
  });
}
// 访问指标
export function getVisitorStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=visitorStatisticsStrategy",
    method: "get",
  });
}
// 评论指标
export function getCommentStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=commentStatisticsStrategy",
    method: "get",
  });
}
// 举报指标
export function getAppealStatisticsStrategy() {
  return request({
    url: "/szpt/statistics?code=appealStatisticsStrategy",
    method: "get",
  });
}

// 获取用户折线图
export function getUserStatistic(data) {
  return request({
    url: "/szpt/statistics",
    method: "get",
    params: {
      code: "newUserLineChartStatisticsStrategy",
      statisticsDate: data.statisticsDate,
      DateType: data.DateType,
    },
  });
}
// 访客折线图
export function getVistorStatistic(data) {
  return request({
    url: "/szpt/statistics",
    method: "get",
    params: {
      code: "newPostLineChartStatisticsStrategy",
      statisticsDate: data.statisticsDate,
      DateType: data.DateType,
    },
  });
}
