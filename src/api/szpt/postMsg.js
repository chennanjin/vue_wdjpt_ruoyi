import request from "@/utils/request.js";
// 发布信息的js文件
export function posTitelImg(imgFile, options) {
  const formData = new FormData();
  formData.append("imgFile", imgFile);
  // 添加选项参数
  if (options) {
    Object.keys(options).forEach((key) => {
      formData.append(key, options[key]);
    });
  }
  return request.post("szpt/file/uploadImage", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

export function uploadImages(multipartFiles, options) {
  const formData = new FormData();
  let tokenStr = window.localStorage.getItem("tokenStr");
  if (multipartFiles) {
    formData.append("multipartFiles", multipartFiles);
  }
  if (options) {
    Object.keys(options).forEach((key) => {
      formData.append(key, options[key]);
    });
  }
  return request({
    url: "/file/uploadImageList",
    method: "post",
    data: formData,
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: tokenStr,
    },
  });
}
