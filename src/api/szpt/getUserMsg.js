import request from "@/utils/request.js";

export function getUserMsg() {
  return request({
    url: "szpt/user?id=",
    method: "get",
  });
}

export function getUserMsgByID(id) {
  return request({
    url: "szpt/user?id=" + id,
    method: "get",
  });
}
