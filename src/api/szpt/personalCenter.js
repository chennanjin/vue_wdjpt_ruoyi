import request from "@/utils/request.js";
// import { getToken } from "@/utils/auth.js";

// // 获取已收藏的列表
// export function parse_collect_concern(userId, pageNum, pageSize) {
//   return request({
//     url: `szpt/post/getListByAction?userId=${userId}&pageNum=${pageNum}&pageSize=${pageSize}0&relationType=SZPT_SZPOST_SYSUSER_COLLECT`,
//     method: "post",
//   });
// }
// 获取已发布的帖子列表
export function getPostList(userId, pageNum, pageSize) {
  return request({
    url: `/szpt/post/list?userId=${userId}&pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC`,
    method: "get",
  });
}
// 获取不同关系状态列表
export function getStatusList(userId, pageNum, pageSize, status) {
  return request({
    url: `szpt/post/getListByAction?userId=${userId}&pageNum=${pageNum}&pageSize=${pageSize}&relationType=${status}`,
    method: "get",
  });
}
// 获取关注列表
export function getFellowList(userId, pageNum, pageSize) {
  return request({
    url: `szpt/relation/followerList?pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC&userId=${userId}`,
    method: "get",
  });
}
// 获取粉丝列表
export function getFansList(userId, pageNum, pageSize) {
  return request({
    url: `szpt/relation/fansList?pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC&userId=${userId}`,
  });
}
export function updateUserData(data) {
  return request({
    url: "/szpt/user/edit",
    method: "put",
    data: data,
  });
}

// 获取个人隐私设置
export function getPrivateSetting(userId) {
  return request({
    url: `/szpt/config/getByEntity?entityId=${userId}&type=SYSUSER_PERSONAL_CENTER`,
    method: "get",
  });
}

// 保存隐私配置
export function savePrivateSetting(data) {
  return request({
    url: "/szpt/config",
    method: "post",
    params: {
      entityId: data.entityId,
      type: "SYSUSER_PERSONAL_CENTER",
      content: JSON.stringify(data.content),
    },
  });
}

// 修改隐私配置
export function updatePrivateSetting(data) {
  return request({
    url: `/szpt/config`,
    method: "put",
    params: {
      id: data.id,
      entityId: data.entityId,
      type: "SYSUSER_PERSONAL_CENTER",
      content: JSON.stringify(data.content),
    },
  });
}
