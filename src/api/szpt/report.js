import request from "@/utils/request.js";

export function reportPost(data) {
  console.log("data", data);
  const fmtData = {
    type: data.type,
    entityId: data.entityId,
    entityType: data.entityType,
    content: data.content,
    reason: data.reason,
  };
  let formData = new FormData();
  for (let key in fmtData) {
    if (fmtData.hasOwnProperty(key)) {
      formData.append(key, fmtData[key]);
    }
  }
  return request.post("/szpt/feedback", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

// 后台获取举报列表
export function getPostListReport(data) {
  return request({
    url: `/szpt/feedback/list`,
    params: {
      type: "REPORT",
      startTime: data.startTime,
      endTime: data.endTime,
      pageNum: data.pageNum,
      pageSize: data.pageSize,
      sortType: data.sortType,
      state: data.state,
      status: data.status,
    },
    method: "get",
  });
}
