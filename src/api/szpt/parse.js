import request from "@/utils/request.js";
// import { getToken } from "@/utils/auth.js";

// 三连关系接口
export function parse_collect_concern(entityId, relationType) {
  return request({
    url: `szpt/relation?entityId=${entityId}&relationType=${relationType}`,
    method: "post",
  });
}
