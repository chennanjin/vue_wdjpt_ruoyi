import request from "@/utils/request.js";

export function initSource() {
  let tokenStr = window.localStorage.getItem("tokenStr");
  return request({
    url: "szpt/tag/list2",
    methods: "get",
    headers: {
      Authorization: tokenStr,
    },
  });
}
