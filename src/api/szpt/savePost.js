// 这里是保存帖子的接口
import request from "@/utils/request.js";
import { getToken } from "@/utils/auth";
export function savePost(datas) {
  let tokenStr = window.localStorage.getItem("tokenStr");
  return request({
    url: "szpt/post",
    method: "post",
    data: datas,
    headers: {
      Authorization: "Bearer " + getToken(), // 在请求头中添加
    },
  });
}
