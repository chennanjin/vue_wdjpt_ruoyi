import request from "@/utils/request.js";

export function getThumbsList(data) {
    let tokenStr = window.localStorage.getItem("tokenStr");
    return request({
        url: "szpt/notify/list?actionType=LIKE&notifyType=REMIND&pageSize=10&markInfo=",
        method: "post",
        data: data,
        headers: {
            Authorization: tokenStr,
        },
    });
}
