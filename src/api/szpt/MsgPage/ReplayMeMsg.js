import request from "@/utils/request.js";

// 获取消息列表
export function getList(data) {
  return request({
    url: "szpt/notify/list",
    method: "post",
    data: data,
  });
}
// 获取未读消息列表
export function getInfoNum() {
  return request({
    url: "/szpt/notify/readStatistics",
    method: "get",
  });
}
