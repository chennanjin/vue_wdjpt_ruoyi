import request from "@/utils/request.js";

export function getComments(datas) {
  return request({
    url: "szpt/comment/listLiteFlow",
    data: datas,
    method: "post",
  });
}

export function sortById(id) {
  return request({
    url: "szpt/post/" + id,
    method: "get",
  });
}
