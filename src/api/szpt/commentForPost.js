// 评论区评论
import request from "@/utils/request.js";

export function componentForPost(commentData) {
  return request({
    url: "szpt/comment/insert",
    method: "post",
    data: commentData,
  });
}

// 获取评论区评论
export function getComments(PageAndSort) {
  let tokenStr = window.localStorage.getItem("tokenStr");
  return request({
    url: "szpt/comment/listLiteFlow",
    method: "post",
    data: JSON.stringify(PageAndSort),
    headers: {
      Authorization: tokenStr,
    },
  });
}

// 删除评论
export function delComment(commentID) {
  let tokenStr = window.localStorage.getItem("tokenStr");
  return request({
    url: "szpt/comment/deleted/" + commentID,
    method: "delete",
    headers: {
      Authorization: tokenStr,
    },
  });
}
