import request from "@/utils/request.js";
import { getToken } from "@/utils/auth.js";

export function getPostList(navigatorID, pageNum, pageSize) {
  return request({
    url: `/szpt/post/list?tagIds=${navigatorID}&pageNum=${pageNum}&pageSize=${pageSize}&sortType=TIME_DESC`,
    method: "get",
    header: {
      Authorization: "Bearer " + getToken(),
    },
  });
}

// 根据id删除帖子
export function delPost(data) {
  return request({
    url: "/szpt/post/delete",
    method: "post",
    params: {
      id: data,
    },
  });
}
