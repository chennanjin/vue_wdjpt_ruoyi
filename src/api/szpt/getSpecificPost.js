import request from "@/utils/request.js";
// 通过id获得具体帖子
export function getSpecificPostById(id) {
  return request({
    url: `szpt/post/${id}`,
    method: "get",
  });
}
