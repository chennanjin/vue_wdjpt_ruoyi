import request from "@/utils/request.js";
// 初始化帖子
export function getPostByID(postID) {
  let tokenStr = window.localStorage.getItem("tokenStr");
  return request({
    url: `szpt/post/${postID}`,
    method: "get",
    headers: {
      Authorization: tokenStr,
    },
  });
}
