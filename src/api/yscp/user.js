import request from '@/utils/request'

// 查询用户列表
export function listYscpUser(query) {
  return request({
    url: '/yscp/bg/users',
    method: 'get',
    params: query,
    baseURL: process.env.VUE_APP_BASE_MOCK_API
  })
}

export function changeYscpUserStatus(status,endTime){
  console.log(endTime)
  let query;
  if(endTime ===undefined){
    query = {
      status
    }
  }else{
    query = {
      status,
      endTime
    }
  }
  return request({
    url: '/yscp/bg/user/status',
    method: 'put',
    params: query,
    baseURL: process.env.VUE_APP_BASE_MOCK_API
  })
}

export function resetYscpUserPwd(userId, password){
  const data = {
    userId,
    password
  }
  return request({
    url: '/yscp/bg/user/resetPwd',
    method: 'put',
    params: data,
    baseURL: process.env.VUE_APP_BASE_MOCK_API
  })
}

export function vipTypeTreeSelect(){
  return request({
    url: '/yscp/bg/user/vipTypeTree',
    method: 'get',
    baseURL: process.env.VUE_APP_BASE_MOCK_API
  })
}


export function updateYscpUserType(userId,vipType){
  return request({
    url: '/yscp/bg/user/vipType',
    method: 'put',
    params: {
      userId,
      vipType
    },
    baseURL: process.env.VUE_APP_BASE_MOCK_API
  })
}
