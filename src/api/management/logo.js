export const LOGO =
    "_________     _____________                \n" +
    "__  ____/___________  /__(_)_____________ _\n" +
    "_  /    _  __ \  __  /__  /__  __ \_  __ `/\n" +
    "/ /___  / /_/ / /_/ / _  / _  / / /  /_/ / \n" +
    "\____/  \____/\__,_/  /_/  /_/ /_/_\__, /  \n" +
    "                                    /____/ \n";
export const MOTTO =
    "         Coding is love, coding until the world is full of love!";
