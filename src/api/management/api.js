import router from '../../router/index';
import axios, { Axios } from 'axios';
// import Qs from "qs";
import { LOGO, MOTTO } from './logo'
import { getToken } from '../../utils/auth'
axios.defaults.timeout = 90000;
// axios.defaults.baseURL = 'http://120.79.190.94:8091';
axios.defaults.baseURL = process.env.VUE_APP_BASE_API;
const Token = getToken();
const api = {
    op(data) {
        const queryParams = new URLSearchParams();
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                queryParams.append(key, data[key]);
            }
        }
        return `?${queryParams.toString()}`;
    },
    ljcccc() {
        console.log(MOTTO);
        console.log(LOGO);
    },
    pay() {
        alert(MOTTO);
    },
    //前后台共用-创建党员信息表
    create_member(data) {
        return ajax(`/szpt/memberMsg/insert`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //后台-查询党员信息
    member_informations(data) {
        return ajax(`/szpt/memberMsg/searchByLike`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data,
        })
    },
    //前后台共用-修改党员信息
    modify_member(data) {
        return ajax(`/szpt/memberMsg/update`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data,
        })
    },
    //连接党员信息
    connect_member(id) {
        return ajax(`/szpt/memberMsg/search2/${id}`, 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        });
    },
    //后台—党员信息导出
    export_member(id) {
        return ajax(`/szpt/memberMsg/export/${id}`, 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        });
    },
    //前后台共用-删除党员信息
    del_member(id) {
        return ajax(`/szpt/memberMsg/delete/${id}`, 'delete', {
            headers: {
                Authorization: "Bearer " + Token,
            },

        });
    },
    //后台-导出所有党员信息表
    export_members() {
        return ajax(`/szpt/memberMsg/exportall`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            responseType: "blob"
        });
    },
    member_informations1(data) {
        // console.log(fullUrl);
        return ajax(`/szpt/memberMsg/searchall` + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        });
    },
    //党员缴费信息插入
    insert_paymessage(data) {
        return ajax(`/szpt/partydue/insert`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //党员缴费信息查询
    query_paymessage() {
        return ajax(`/szpt/partydue/search/102`, 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-查询缴费信息
    background_query_paymessage(data) {
        return ajax(`/szpt/partydue/searchByLike`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //后台-查询党费核准信息
    background_apporve_paymessage(data) {
        return ajax(`/szpt/duecount/search`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //后台-所有党费核准信息导出
    background_query_allpaymessage(data) {
        return ajax(`/szpt/duecount/exportAllData`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //后台-个人党费核准信息导出
    background_import_paymessageofpersonal(id) {
        return ajax(`/szpt/duecount/exportById/${id}`, 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-连接党费核准信息
    background_connect_apporvemessage() {
        return ajax(`/szpt/duecount/searchById/党员名字2`, 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-导入党员党费核准表
    background_import_pay_stdtable(data) {
        return ajax(`/szpt/duecount/importData`, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
    //后台-打卡任务列表
    checkin_list(data) {
        return ajax('szpt/checkIn/list' + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-打卡详情
    checkin_detail(data) {
        return ajax(`/szpt/checkIn` + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-打卡任务下已完成打卡的用户列表?
    checkin_finish_members(data) {
        return ajax('/szpt/checkIn/userList' + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-打卡任务保存
    checkin_save(data) {
        const queryParams = new URLSearchParams();
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                queryParams.append(key, data[key]);
            }
        }
        const fullUrl = `?${queryParams.toString()}`;
        return ajax('szpt/checkIn' + fullUrl, 'post', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-列表
    feedback_list(data) {
        return ajax('/szpt/feedback/list' + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            },
        })
    },
    //后台-详情
    feedback_detail(data) {
        return ajax('/szpt/feedback' + this.op(data), 'get', {
            headers: {
                Authorization: "Bearer " + Token,
            }
        })
    },
    feedback_handle(data) {
        return ajax('/szpt/feedback', 'put', {
            headers: {
                Authorization: "Bearer " + Token,
            },
            data
        })
    },
}

export default api;
function ajax(url, method, options) {
    if (options !== undefined) {
        var { params = {}, data = {}, headers = {} } = options
    } else {
        params = data = {}
    }
    return new Promise((resolve, reject) => {
        axios({
            url,
            method,
            params,
            data,
            // transformRequest: [function (data) {
            //     // 对 data 进行任意转换处理
            //     return Qs.stringify(data)
            // }],
            headers,
        }).then((res) => {
            console.log(res);
            resolve(res)
        }).catch(error => {
            reject(error)
        })
    })
}