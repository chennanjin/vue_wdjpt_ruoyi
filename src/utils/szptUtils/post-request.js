// 封装响应拦截器，每次发送请求之前都会通过响应拦截器
import axios from "axios";
import router from "@/router";
import { Message } from "element-ui";

// 配置请求拦截器
axios.interceptors.request.use(
  (config) => {
    if (window.sessionStorage.getItem("tokenStr")) {
      // 将本地的令牌放到请求头里面，每次请求的时候都会携带令牌
      config.headers["Authorization"] =
        window.sessionStorage.getItem("tokenStr");
    }
    // console.log(JSON.stringify(config) + "11");
    return config;
  },
  (error) => {
    console.log(error);
  }
);
// 配置响应拦截器
axios.interceptors.response.use(
  (success) => {
    // 响应成功业务逻辑
    if (success.status && success.status == 200) {
      if (
        success.data.code == 500 ||
        success.data.code == 401 ||
        success.data.code == 403
      ) {
        /**
             * 200：请求成功。
            301：永久重定向。
            400：客户端请求错误。
            403：权限禁止
            404：资源未找到。
            500：服务器内部错误。
             */
        // 业务错误
        // console.log("1");
        Message.error({ message: success.data.msg });
        return;
      }
      if (success.data.msg) {
        // 没有错误信息就是成功信息，提示一下成功信息
        // console.log("2");
        Message.success({ message: success.data.msg });
      }
    }
    // 返回信息，我们才可以拿到响应信息，这只是响应拦截器
    // console.log(success.data + "12312");
    return success.data;
  },
  (error) => {
    // 服务器宕机或者响应失败的拦截器
    if (error.response.code == 504 || error.response.code == 404) {
      Message.error({ message: "服务器被吃了！请稍后重试！" });
    } else if (error.response.code == 403) {
      Message.error({ message: "当前用户权限不足！请联系管理员！" });
    } else if (error.response.code == 401) {
      Message.error({ message: "当前用户尚未登录请登录！" });
      // 用户未登录的情况下会跳转到登录页面
      router.replace("/szptLogin/login");
    } else {
      if (error.response.data.message) {
        // 如果有响应回来的错误信息就打印响应回来的错误信息
        Message.error({ message: error.response.data.msg });
      } else {
        // 如果连响应信息都没有就打印未知错误
        Message.error({ message: "未知错误！" });
      }
    }
    return;
  }
);

//封装post请求拦截器
let base = "";
export const postRequest = (url, params) => {
  return axios({
    method: "post",
    url: `${base}${url}`,
    data: params,
  });
};
// 封装get请求拦截器
export const getRequest = (url, params) => {
  return axios({
    method: "get",
    url: `${base}${url}`,
    data: params,
  });
};

// 封装put请求拦截器
export const putRequest = (url, params) => {
  return axios({
    method: "put",
    url: `${base}${url}`,
    data: params,
  });
};

// 封装delete请求拦截器
export const deleteRequest = (url, params) => {
  return axios({
    method: "delete",
    url: `${base}${url}`,
    data: params,
  });
};
