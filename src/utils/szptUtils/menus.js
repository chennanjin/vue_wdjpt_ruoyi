import { getRequest } from "./post-request";

//封装菜单请求工具类
export const initMenu = (router, store) => {
  // if (store.state.routes.length > 0) {
  //   // 先判断stoure里面是否有路由，有路由的话就不用发请求
  //   return;
  // }
  getRequest("szpt/tag/list1").then((data) => {
    let datas = JSON.parse(JSON.stringify(data.data)).data;

    console.log("菜单工具类："+data.data);
    // console.log(datas.data);
    if (datas) {
      //   let fmtRoutes = datas;
      //   添加到路由，并将数据存储到vuex
      store.commit("initRoutes", datas);
      //   console.log(datas);
    }
  });
};
export const farmatRoutes = (routes) => {
  let fmtRoutes = [];
  // 初始化
  routes.forEach((router) => {
    let { szTag, childrenlist } = router;
    if (childrenlist && childrenlist instanceof Array) {
      // 递归
      childrenlist = farmatRoutes(childrenlist);
    }
    let fmtRouter = {
      szTag: szTag,
      childrenlist: childrenlist,
    };
    fmtRoutes.push(fmtRouter);
  });
  return fmtRoutes;
};
