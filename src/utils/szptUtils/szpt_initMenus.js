// 引入初始化菜单请求链接
import { initMenus } from "../../api/szpt/initMenus";

//封装菜单初始化工具
/**
 * @function initMenu 这是一个菜单初始化函数,通过导航栏的菜单RoutesTag.szptRoutes(navRouter)和tag标签初始化数据
 * @param RoutesTag {object} RoutesTag.szptRoutes(导航栏全部路由)，RoutesTag.tagList导航栏下面全部的标签
 *
 */
export async function initMenu(store, router) {
  await initMenus()
    .then((resp) => {
      let datas = resp.data.data;
      // 拿到后端返回的数据并对后端数据进行格式化
      // console.log("拿到的数据为： ", datas);
      if (datas) {
        let RoutesTag = formatRoutes(datas);
        store.commit("menus/INIT_MENUS", RoutesTag.szptRoutes);
        store.commit("menus/INIT_TAGLIST", RoutesTag.tagList);

        // 会话保存当前导航栏路由和当前tagList子tag
        if (
          !window.sessionStorage.getItem("navRouter") &&
          !window.sessionStorage.getItem("tagObject")
        ) {
          // 初始化的时候保存导航栏第一个对象
          window.sessionStorage.setItem(
            "navRouter",
            JSON.stringify(RoutesTag.szptRoutes[0])
          );
          // 初始化的时候保存第一个tagObject对象
          window.sessionStorage.setItem(
            "tagObject",
            JSON.stringify(RoutesTag.tagList[0].childrenlist[0])
          );
        }

        // 本地保存全部导航栏
        window.localStorage.setItem(
          "menusRoutes",
          JSON.stringify(RoutesTag.szptRoutes)
        );
        // 保存导航栏的全部标签
        window.localStorage.setItem(
          "tagList",
          JSON.stringify(RoutesTag.tagList)
        );
      }
      return true;
    })
    .catch((err) => {
      console.log("打印了错误信息" + err);
    });
}
export const formatRoutes = (datas) => {
  let RoutesTag = {
    szptRoutes: [],
    tagList: [],
  };
  datas.forEach((router) => {
    // 这里判断并取出导航栏标签
    if (router.szTag && router.szTag.type == "NAVIGATION") {
      let szTagList = {
        id: router.szTag.id,
        name: router.szTag.name,
        path: router.szTag.path,
      };
      RoutesTag.szptRoutes.push(szTagList);
    }
    if (router.szTag && router.szTag.type == "LABEL" && router.childrenlist) {
      let fmtchildrenlists = fmtchildrenlist(router.childrenlist);
      let list = {
        id: router.szTag.id,
        name: router.szTag.name,
        path: router.szTag.path,
        // 在这里定义一个数组children并赋值router.children
        childrenlist: fmtchildrenlists,
      };
      RoutesTag.tagList.push(list);
    }
  });
  // 这里将tagList存储到本地方便发布帖子的时候访问
  window.localStorage.setItem("tagList", RoutesTag.tagList);
  return RoutesTag;
};

export const fmtchildrenlist = (datas) => {
  // 在这里将子列表格式化
  let fmtchildren = [];
  if (datas && datas instanceof Array) {
    datas.forEach((children) => {
      if (children) {
        let child = {
          id: children.id,
          name: children.name,
          path: children.path,
        };
        fmtchildren.push(child);
      }
    });
  } else {
    console.log("子列表不是数组或子列表数据不存在");
  }
  // 将格式化好的数据返回
  return fmtchildren;
};
