import { getPostsPublic } from "@/utils/szptUtils/getPostLists.js";
// 封装一个路由ID匹配工具类，用户点击每次进入不同的路由导航都能达到初始化当前路由界面的数据
/**
 * @function upDatePageMsg
 * @param navigatorID {string}路由导航ID
 * @description menusRoutes(导航栏所有的路由) tagList（标签栏所有的标签）
 */
export function initPageMsg(navigatorID, flag) {
  // 用户获取当前导航的id和标签的第一个id达到初始化页面的目的

  let sessionStorageNavID = window.sessionStorage.getItem("navigatorID");
  // console.log("sessionID", sessionStorageNavID);
  // console.log("navID", navigatorID);
  let menusRoutes = JSON.parse(window.localStorage.getItem("menusRoutes"));
  let tagList = JSON.parse(window.localStorage.getItem("tagList"));
  if (navigatorID || flag) {
    // 当切换导航栏的时候
    // 当通过点击导航栏来切换数据的时候，通过点击时候传过来的navigatorID实现数据切换
    let navRouter = matchRouterByID(navigatorID, menusRoutes);
    // console.log("匹配到的数据，", flag, navRouter);
    window.sessionStorage.setItem("navRouter", JSON.stringify(navRouter));
    window.sessionStorage.setItem(
      "tagObject",
      JSON.stringify(tagList[0].childrenlist[0])
    );
    // 如果当前的路由id与会话中存储的id匹配说明已经切换路由了，可以更新初始化界面的数据
    window.sessionStorage.setItem("navigatorID", navigatorID);
    // 更新请求数据
    if (flag) {
      (async () => {
        getPostsPublic(menusRoutes[0].id, tagList[0].childrenlist[0].id, 1, 10);
      })();
    } else {
      (async () => {
        getPostsPublic(navigatorID, tagList[0].childrenlist[0].id, 1, 10);
      })();
    }
    window.sessionStorage.setItem("pageNum", 1);
    window.sessionStorage.setItem("pageSize", 10);
  } else {
    // 当刷新浏览器的时候,获取保存在本地会话缓存中的路由路由导航然后对数据赋值
    let navRouter = JSON.parse(window.sessionStorage.getItem("navRouter"));
    let tagObject = JSON.parse(window.sessionStorage.getItem("tagObject"));
    let pageNum = window.sessionStorage.getItem("pageNum");
    let pageSize = window.sessionStorage.getItem("pageSize");

    (async () => {
      if (navRouter) {
        await getPostsPublic(navRouter.id, tagObject.id, pageNum, pageSize);
      } else {
        getPostsPublic(menusRoutes[0].id, tagList[0].childrenlist[0].id, 1, 10);
      }
    })();
    // console.log("草，浏览器", navRouter, tagObject, pageNum, pageSize);
  }
}
// 用户只是点击刷新数据
export function upDatePageMsg() {}

export function matchRouterByID(navigatorID, menusRoutes) {
  if (menusRoutes && menusRoutes instanceof Array) {
    const matchRouter = menusRoutes.find((router) => router.id === navigatorID);
    if (matchRouter) {
      // console.log("匹配到了");
      return matchRouter;
    } else {
      // console.log("没有匹配到");
      return null;
    }
  }
}
