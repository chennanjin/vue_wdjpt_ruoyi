import store from "@/store";
// 评论区评论
import {
  componentForPost,
  getComments,
  delComment,
} from "@/api/szpt/commentForPost.js";
// 评论区评论
/**
 * @description 主评论区位置评论的一个参数
 * @param commentData{object} 评论时候传过来的评论参数
 */
export function comment(commentData, ComIndex) {
  // console.log("接收的评论信息", commentData);
  return new Promise((resolve, reject) => {
    componentForPost(commentData)
      .then((resp) => {
        if (resp.code === 200) {
          // console.log("评论信息", resp.data);
          console.log("评论回显数据", resp.data);
          // 当ComIndex存在则是二级评论,在对应的评论列表下插入
          if (ComIndex >= 0) {
            let insCommentDataMsgData = {
              ComIndex: ComIndex,
              commentData: resp.data.data,
            };
            // console.log("二级评论");
            store.commit(
              "commentList/INCREASE_COMMETS_INCOMMENTS",
              insCommentDataMsgData
            );
          } else {
            store.commit("commentList/INCREASE_COMMETS", resp.data);
            // console.log("跑这里俩了");
          }
          resolve(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
}
// 获取帖子评论区数据
export function getCommentsList(getCommentListsParams) {
  // console.log("接收的评论参数", getCommentListsParams);
  return new Promise((resolve, reject) => {
    getComments(getCommentListsParams)
      .then((resp) => {
        if (resp.code === 200) {
          window.sessionStorage.setItem(
            "commentsList",
            JSON.stringify(resp.data)
          );
          // 将评论信息存进vuex方便管理
          store.dispatch("commentList/init_commentS", resp.data.data);
          resolve(true);
        } else {
          console.log("请求失败！");
          reject(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
}

// 根据用户评论id删除用户评论
export function delUserComment(commentID, ComIndex) {
  return new Promise((resolve, reject) => {
    let flag = true;
    delComment(commentID)
      .then((resp) => {
        if (resp.code === 200) {
          resolve(flag);
          // console.log("删除的数据为", resp.data);
          if (ComIndex >= 0) {
            let delCommentDataMsg = {
              ComIndex: ComIndex,
              commentData: resp.data,
            };
            store.commit(
              "commentList/DEL_COMMENTSDATA_SECOND",
              delCommentDataMsg
            );
          } else {
            store.commit("commentList/DEL_USERDATAMSG", resp.data);
          }
        } else {
          flag = !flag;
          reject(flag);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
}

/**
 * @description 发布评论
 */
export function postCommentMsg(ComementData, thisVM) {
  let flag = true;
  const currentTime = fmtDate();
  if (ComementData) {
    let comment = {
      id: "",
      createTime: currentTime,
      updateTime: currentTime,
      createUser: "",
      updateUser: "",
      status: 0,
      remark: null,
      createUsername: "",
      createUserHeadshotUrl: null,
      content: "",
      parentId: null,
      targetId: null,
      entityId: "",
      entityType: "",
      targetUserId: null,
      targetUsername: null,
      parsecount: "0",
      replycount: "3",
      score: null,
    };
  } else {
    return !flag;
  }
}
// 格式化系统时间
export function fmtDate() {
  const currentTime = new Date();
  const year = currentTime.getFullYear();
  const month = (currentTime.getMonth() + 1).toString().padStart(2, "0");
  const day = currentTime.getDate().toString().padStart(2, "0");
  const hour = currentTime.getHours().toString().padStart(2, "0");
  const minute = currentTime.getMinutes().toString().padStart(2, "0");
  const second = currentTime.getSeconds().toString().padStart(2, "0");
  const formattedTime = `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  return formattedTime;
}
