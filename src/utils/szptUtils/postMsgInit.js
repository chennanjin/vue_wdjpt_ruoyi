import { posTitelImg, uploadImages } from "@/api/szpt/postMsg.js";
import { BASE_URL } from "@/plateFormTag.js";

export function postMsgInit(FileImg, imgArray) {
  const options = {
    compressQualityFlag: true,
    saveOriginalQualityFlag: true,
    waterMarkFlag: true,
    OriginUrlPass: "",
    lowQuilUrlPass: "",
  };
  posTitelImg(FileImg, options)
    .then((resp) => {
      if (resp.data) {
        options.OriginUrlPass =
          BASE_URL.lookThroughPicurl + resp.data.originalQualityPaths[0];
        options.lowQuilUrlPass =
          BASE_URL.lookThroughPicurl + resp.data.lowQualityPaths[0];
        // titlePicFile.OriginUrlPass = BASE_URL.lookThroughPicurl + OriginUrlPass;
        // titlePicFile.lowQuilUrlPass =
        //   BASE_URL.lookThroughPicurl + lowQuilUrlPass;
        imgArray.push(options);
        // 将上传得到的图片地址返回给表单
      } else {
        console.log("图片不存在！");
      }
    })
    .catch((err) => {
      console.log(err + "图片上传错误");
    });
}

// 图片批量上传操作
export function uploadImageList(multipartFiles, coursePics) {
  const options = {
    compressQualityFlag: true,
    saveOriginalQualityFlag: true,
    waterMarkFlag: true,
  };

  uploadImages(multipartFiles, options)
    .then((resp) => {
      let piclist = {
        OriginUrlPass:
          BASE_URL.lookThroughPicurl + resp.data.originalQualityPaths[0],
        lowQuilUrlPass:
          BASE_URL.lookThroughPicurl + resp.data.lowQualityPaths[0],
      };
      // console.log("piclist " + piclist);
      coursePics.push(piclist);
    })
    .catch((err) => {
      console.log(err + "图片批量上传错误");
    });
}
