// 第一次进入评论区的时候初始化评论区列表
// 引入更新评论区列表文件
import { sort } from "core-js/core/array";
import { upDateCommentLists } from "./upDateCommentList";

export function initCommentList(entityId, entityType) {
  // comments收收返回的评论区列表
 // 刚开始评论区默认加载10条评论，每条评论里面有5条子评论  
  let comments = upDateCommentLists(entityId, entityType, 0, 10, 0, 5, sort);

}
