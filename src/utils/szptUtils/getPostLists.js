import { getPostList } from "@/api/szpt/getPostList.js";
import store from "@/store";
export function getPostsPublic(navigatorID, pageNum, pageSize, dataList) {
  return getPostList(navigatorID, pageNum, pageSize)
    .then((resp) => {
      if (resp.data.pageInfo) {
        dataList = resp.data.pageInfo.list;
        // store.dispatch("soure/initSoucesMinListData", pageInfoList);
      } else {
        // store.dispatch("soure/initSoucesMinListData", []);
      }
    })
    .catch((err) => {
      console.log("获取帖子列表数据错误", err);
    });
}

// 获取当前导航栏的id
export function getNavRouterId(routerPath) {
  let menusRoutes = JSON.parse(window.localStorage.getItem("menusRoutes"));
  let RouterID = "";
  if (menusRoutes.length > 0) {
    menusRoutes.forEach((router) => {
      if (router.path == routerPath) {
        RouterID = router.id;
      }
    });
  }
  return RouterID;
}
