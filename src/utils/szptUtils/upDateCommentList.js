// 这里用来更新更新评论区列表
// 引入更新评论区api
import { initCommentList } from "../../api/szpt/initCommentList";
import { sortById } from "../../api/szpt/initCommentList";

export function upDateCommentLists(
  entityId,
  entityType,
  parentUp,
  parentDown,
  childrenUp,
  childrenDown,
  sort
) {
  initCommentList({
    // 帖子id
    entityId: entityId,
    // 帖子分类(帖子类型，视频/图片形象)
    entityType: entityType,
    //当前评论列表上限
    parentup: parentUp,
    //当前的评论列表下限
    parentdown: parentDown,
    //当前子列表评论上限
    childrenup: childrenUp,
    //当前评论子列表下限
    childrendown: childrenDown,
    // 排序方式
    sort: sort,
  })
    .then((resp) => {
      let comments = resp.data.data;
      console.log("当前响应的评论数据为", comments);
      // 将请求得到的数据返回
      return comments;
    })
    .catch((err) => {
      // 打印请求发出后的错误信息
      console.log(err);
    });
}

// 第一次进入评论区的时候初始化评论区列表
// 通过用户传过来的id判断帖子的类型，并请求属鼠
export function initCommentLists(entityId) {
  // comments收收返回的评论区列表
  // 刚开始评论区默认加载10条评论，每条评论里面有5条子评论
  let entityType = fmtidDatas(entityId);
  //   let comments = upDateCommentLists(entityId, entityType, 0, 10, 0, 5, sort);
}

export function fmtidDatas(id) {
  sortById(id)
    .then((resp) => {
      console.log("id返回值" + resp);
    })
    .catch((err) => {
      console.log("搜索的id不存在或出现错误！");
    });
}
