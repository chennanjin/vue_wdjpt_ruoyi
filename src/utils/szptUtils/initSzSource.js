import { getPostByID } from "@/api/szpt/initPost.js";
import store from "@/store";

export function initPostByID(postID) {
  console.log("帖子id", postID);
  return getPostByID(postID)
    .then((resp) => {
      if (resp.msg == "操作成功" && resp.code == 200) {
        store.dispatch("szSource/init_SOURCEDATA", resp.data);
        window.sessionStorage.setItem(
          "postDataByID",
          JSON.stringify(resp.data)
        );
        // console.log("输出一下获得的帖子数据", resp.data);
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
