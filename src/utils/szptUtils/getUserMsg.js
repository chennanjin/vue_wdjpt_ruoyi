import { getUserMsg } from "@/api/szpt/getUserMsg.js";
import store from "@/store";

export function getUserMsgs() {
  // getUserMsg()
  //   .then((resp) => {
  //     let userMsg = resp.data;
  //     if (userMsg) {
  //       store.dispatch("userData/INIT_USERDATAMSG", userMsg);
  //     } else {
  //       console.log("个人信息初始化失败！");
  //     }
  //   })
  //   .catch((err) => {
  //     console.log("获取个人信息失败");
  //   });
  return new Promise((resolve, reject) => {
    getUserMsg()
      .then((resp) => {
        if (resp.code == 200) {
          let userMsg = resp.data;
          store.dispatch("userData/init_userDataMsg", userMsg);
          // console.log("获取用户信息",resp.data);
          resolve(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
}
