import Vue from "vue";
import VueRouter from "vue-router";
// 菜单初始化函数
import store from "../../store";
import router from "..";
import { initMenu } from "../../utils/szptUtils/szpt_initMenus";
import { initPostByID } from "@/utils/szptUtils/initSzSource.js";
import { initPageMsg } from "@/utils/szptUtils/upDataMsg.js";
import { getUserMsgs } from "@/utils/szptUtils/getUserMsg.js";
import {
  upDateCommentLists,
  initCommentLists,
} from "../../utils/szptUtils/upDateCommentList";

Vue.use(VueRouter);

const szptRouter = [
  // {
  //   name: "szptHome",
  //   path: "",
  //   component: () => import("@/views/szptProjectPage/szptHomePage/szptHome"),
  //   meta: {
  //     requireAuth: false,
  //     navigatorID: "",
  //   },
  //   beforeEnter: (to, from, next) => {
  //     if (to.path == "") {
  //       if (from.path == "") {
  //         (async () => {
  //           // await initPageMsg(to.meta.navigatorID, true);
  //           await getUserMsgs();
  //           console.log("@222");
  //           // next("/szptHome?id=191");
  //           next();
  //         })();
  //       }
  //       (async () => {
  //         await getUserMsgs();
  //         // next("/szptHome?id=191");
  //         // console.log("2222");
  //       })();
  //       next();
  //     }
  //   },
  // },
  {
    // 登录路由
    path: "/szptLogin",
    hidden: true,
    component: () => import("@/views/szptProjectPage/szptLoginPage/SzptLogin"),
    beforeEnter: (to, from, next) => {
      next();
    },
    meta: {
      titrle: "思政平台登录注册",
      requireAuth: false,
    },
    children: [
      {
        path: "/szptLogin/login",
        hidden: true,
        component: () =>
          import("@/views/szptProjectPage/szptLoginPage/UserLogin"),
        meta: {
          requireAuth: false,
          title: "登录",
        },
      },
      {
        path: "/szptLogin/register",
        hidden: true,
        component: () =>
          import("@/views/szptProjectPage/szptLoginPage/UserRegister"),
        meta: {
          requireAuth: false,
          title: "注册",
        },
      },
    ],
  },
  {
    // 主页面路由
    path: "/szptPlate",
    hidden: true,
    component: () => import("@/views/szptProjectPage/szptHomePage/szptPlate"),
    beforeEnter: (to, from, next) => {
      // 如果是从登录页面访问这个路由思政平台路由时候的前置守卫
      // 进入该页面时候调用菜单初始化方法初始菜单
      if (from.path == "/szptLogin/login") {
        (async () => {
          // await initMenu(store, router);
        })();
      }
      // 等待异步函数执行完毕再next

      next();
    },
    meta: {
      // title: "思政平台首页",
      requireAuth: false,
    },
    children: [
      {
        name: "szptHome",
        path: "/szptHome",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptHome"),
        meta: {
          requireAuth: false,
          navigatorID: "",
        },
        beforeEnter: (to, from, next) => {
          if (to.path == "/szptHome") {
            // to.meta.navigatorID = to.params.navigatorID;
            // console.log("navigatorID", to.meta.navigatorID);
            // initPageMsg(to.meta.navigatorID, false);
            if (from.path == "/szptLogin/login") {
              (async () => {
                // await initPageMsg(to.meta.navigatorID, true);
                // await getUserMsgs();
                // next("/szptHome?id=191");
                next();
              })();
            }
            // await getUserMsgs();
            // next("/szTeacher?id=192");
            next();
          }
        },

        children: [
          {
            path: "/tagListContain/:id",
            component: () =>
              import("@/components/szptPage/szptHome/tagListContain"),
            meta: {
              requireAuth: false,
            },
          },
        ],
      },
      {
        path: "/szTeacher",
        name: "szTeacher",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szTeacher"),
        meta: {
          requireAuth: false,
          navigatorID: "",
        },
        beforeEnter: (to, from, next) => {
          // to.meta.navigatorID = to.params.navigatorID;
          // console.log("navigatorID", to.meta.navigatorID);
          // initPageMsg(to.meta.navigatorID);
          next();
        },
      },
      {
        path: "/szptMaterial",
        name: "szptMaterial",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptMaterial"),
        meta: {
          requireAuth: false,
          navigatorID: "",
        },
        beforeEnter: (to, from, next) => {
          // to.meta.navigatorID = to.params.navigatorID;
          // console.log("navigatorID", to.meta.navigatorID);
          // initPageMsg(to.meta.navigatorID);
          next();
        },
      },
      {
        path: "/szptCase",
        name: "szptCase",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptCase"),
        meta: {
          requireAuth: false,
          navigatorID: "",
        },
        beforeEnter: (to, from, next) => {
          // to.meta.navigatorID = to.params.navigatorID;
          // console.log("navigatorID", to.meta.navigatorID);
          // initPageMsg(to.meta.navigatorID);
          next();
        },
      },
      {
        path: "/szptResource",
        name: "szptResource",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptResource"),
        meta: {
          requireAuth: false,
          navigatorID: "",
        },
        beforeEnter: (to, from, next) => {
          to.meta.navigatorID = to.params.navigatorID;
          // initPageMsg(to.meta.navigatorID);
          next();
        },
      },
      {
        path: "/szSource",
        // path: "/szSource/:id",
        name: "szSource",
        component: () =>
          import("@/views/szptProjectPage/szptDataPage/szSource"),
        props: true,
        meta: {
          requireAuth: false,
          // 路由里面携带的参数，用来改变标签页
          id: "",
          postDatas: "",
        },
        beforeEnter: (to, from, next) => {
          // 将路由携带的参数赋值到自定义属性中，方便在组件中使用
          // to.meta.id = to.params.id;
          // to.meta.postDatas = to.params.postDatas;
          //根据标签id查找对应的帖子idA

          setTimeout(() => {
            (async () => {
              // await initPostByID(to.meta.id);
              await getUserMsgs();
              next();
            })();
          }, 1000);

          // initCommentLists(sortId);
        },
      },
      // "个人中心"
      {
        path: "/szptPersonalCenter",
        name: "szptPersonalCenter",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptPersonalCenter"),
        meta: {
          requireAuth: false,
        },
        children: [
          {
            path: "/personalAlreadyPost",
            name: "personalAlreadyPost",
            component: () =>
              import(
                "@/components/szptPage/szPersonalcenter/personalAlreadyPost"
              ),
            meta: {
              requireAuth: false,
            },
          },
          {
            path: "/personalCollected",
            name: "personalCollected",
            component: () =>
              import(
                "@/components/szptPage/szPersonalcenter/personalCollected"
              ),
            meta: {
              requireAuth: false,
            },
          },
          {
            path: "/personalMsg",
            name: "personalMsg",
            component: () =>
              import("@/components/szptPage/szPersonalcenter/personalMsg"),
            beforeEnter: (to, from, next) => {
              (async () => {
                await getUserMsgs();
                next();
              })();
            },
            meta: {
              requireAuth: false,
            },
          },
          // {
          //   path: "/personalPost",
          //   name: "personalPost",
          //   component: () =>
          //     import("@/components/szptPage/szPersonalcenter/personalPost"),
          //   meta: {
          //     requireAuth: false,
          //   },
          //   children: [
          //     {
          //       path: "/personalPost/postPictures",
          //       name: "postPictures",
          //       component: () =>
          //         import(
          //           "@/components/szptPage/szPersonalcenter/postCompents/postPictures"
          //         ),
          //       meta: {
          //         requireAuth: false,
          //       },
          //     },
          //     {
          //       path: "/personalPost/postTags",
          //       name: "postTags",
          //       component: () =>
          //         import(
          //           "@/components/szptPage/szPersonalcenter/postCompents/postTags"
          //         ),
          //       meta: {
          //         requireAuth: false,
          //       },
          //     },
          //   ],
          // },
          {
            path: "/personalSetting",
            name: "personalSetting",
            component: () =>
              import("@/components/szptPage/szPersonalcenter/personalSetting"),
            meta: {
              requireAuth: false,
            },
          },
        ],
        beforeEnter: (to, from, next) => {
          next();
        },
      },
      // "通知"
      {
        path: "/szptMsgCenter",
        name: "szptMsgCenter",
        component: () =>
          import("@/views/szptProjectPage/szptHomePage/szptMsgCenter"),
        meta: {
          requireAuth: false,
        },
        children: [
          {
            path: "/ReplyMeMsg",
            name: "ReplyMeMsg",
            component: () =>
              import("@/components/szptPage/szptMsgPage/ReplyMeMsg"),
            meta: {
              requireAuth: false,
            },
          },
          {
            path: "/SystemMsg",
            name: "SystemMsg",
            component: () =>
              import("@/components/szptPage/szptMsgPage/SystemMsg"),
            meta: {
              requireAuth: false,
            },
          },
          {
            path: "/ThumbsUpMsg",
            name: "ThumbsUpMsg",
            component: () =>
              import("@/components/szptPage/szptMsgPage/ThumbsUpMsg"),
            meta: {
              requireAuth: false,
            },
          },
        ],
      },
      // 发布中心
      {
        path: "/personalPost",
        name: "personalPost",
        component: () =>
          import("@/components/szptPage/szPersonalcenter/personalPost"),
        meta: {
          title: "发布中心",
          requireAuth: false,
        },
        children: [
          {
            path: "/personalPost/postPictures",
            name: "postPictures",
            component: () =>
              import(
                "@/components/szptPage/szPersonalcenter/postCompents/postPictures"
              ),
            meta: {
              title: "发布帖子",
              requireAuth: false,
            },
          },
          {
            path: "/personalPost/postTags",
            name: "postTags",
            component: () =>
              import(
                "@/components/szptPage/szPersonalcenter/postCompents/postTags"
              ),
            meta: {
              requireAuth: false,
            },
          },
        ],
      },
      // 搜索界面
      {
        path: "/search",
        name: "search",
        component: () => import("@/views/szptProjectPage/searchPage/index.vue"),
      },
    ],
  },
];

export default szptRouter;
