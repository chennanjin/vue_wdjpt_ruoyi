import Vue from "vue";
import Vuex from "vuex";
import app from "./modules/app";
import dict from "./modules/dict";
import user from "./modules/user";
import tagsView from "./modules/tagsView";
import permission from "./modules/permission";
import settings from "./modules/settings";
import getters from "./getters";

// 导入szpt的store组件
import menus from "./modules/szpt_store/menus";
import soure from "./modules/szpt_store/soure";
import commentList from "./modules/szpt_store/commentList";
import szSource from "./modules/szpt_store/szSource";
import userData from "./modules/szpt_store/userData";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    dict,
    user,
    tagsView,
    permission,
    settings,

    // szpt组件
    menus,
    soure,
    commentList,
    szSource,
    userData,
  },
  getters,
});

export default store;
