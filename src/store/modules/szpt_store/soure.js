import { initSource } from "../../../api/szpt/initSouces";
import { getSpecificPostById } from "@/store/modules/szpt_store/soure.js";
export default {
  namespaced: true,
  state: {
    // 视频分类资源列表
    SourceList: [],
    // 视频资源列表
    fmtChildrenList: [],
  },
  actions: {
    // 在这里初始化组件传过来的数据并提交
    initSoucesMinListData(contex, pageInfoList) {
      contex.commit("INIT_SOURLIST", pageInfoList.list);
    },
  },
  mutations: {
    INIT_SOURLIST(state, pageInfoList) {
      // 向状态管理栏中提交数据
      state.SourceList = pageInfoList;
    },
    INIT_FMTCHILDRENLIST(state, fmtchildrenlist) {
      state.fmtChildrenList = fmtchildrenlist;
    },
  },
};

// 或去帖子列表后通过帖子id得到响应的帖子数组
export function getSpecificPost(SpecificPostLists) {
  let SpecificArry = [];

  if (SpecificPostLists.length > 0) {
    SpecificPostLists.forEach((list) => {
      let contentPostMsg = list.data.content;

      console.log("list", list);
      let aPostList = {
        // 计数
        countDTO: list.countDTO,
        // 发布的tag标签
        tagDTOList: list.tagDTOList,
        // 发布的用户信息
        userDTO: list.userDTO,
        // 发布时候包含的内容
        content: contentPostMsg,
      };
      SpecificArry.push(aPostList);
    });
  }
  return SpecificArry;
}
