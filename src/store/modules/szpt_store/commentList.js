// 评论区状态管理栏
export default {
  namespaced: true,
  state: {
    // 评论区，全部评论容器
    allComments: [],
  },
  actions: {
    init_commentS(contex, CommentsList) {
      contex.commit("INIT_ALLCOMMENTS", CommentsList);
    },
  },
  mutations: {
    /**
     * @description 初始化数组参数
     * @params CommentsList 请求到的后端评论数组
     */
    INIT_ALLCOMMENTS(state, CommentsList) {
      // 更新当前评论区列表所有评论
      state.allComments = CommentsList;
    },

    /**
     * @description 增加评论向数组第一位增加评论ins
     * @params 新增的评论
     */
    INCREASE_COMMETS(state, insCommentDataMsg) {
      if (insCommentDataMsg) {
        console.log("获取的评论返回数据是", insCommentDataMsg);
        state.allComments.unshift(insCommentDataMsg);
      }
    },

    /**
     * @description 二级评论增加评论
     */
    INCREASE_COMMETS_INCOMMENTS(state, insCommentDataMsgData) {
      if (insCommentDataMsgData) {
        state.allComments[insCommentDataMsgData.ComIndex].childrenlist.unshift(
          insCommentDataMsgData.commentData
        );
      }
    },
    /**
     * @description 删除评论
     * @params delCommentDataMsg 删除的评论信息
     */
    DEL_USERDATAMSG(state, delCommentDataMsg) {
      if (delCommentDataMsg) {
        if (state.allComments instanceof Array) {
          let commentList = state.allComments;
          let ComIndex = 0;
          for (let index = 0; index < commentList.length; index++) {
            if (commentList[index].data.id === delCommentDataMsg.data.id) {
              ComIndex = index;
            }
          }
          state.allComments.splice(ComIndex, 1);
        } else {
          console.log("删除数据不存在");
        }
      }
    },

    /**
     * @descripton 删除二级评论
     */
    DEL_COMMENTSDATA_SECOND(state, delCommentDataMsg) {
      if (delCommentDataMsg) {
        let delArry =
          state.allComments[delCommentDataMsg.ComIndex].childrenlist;
        let delIndex = 0;
        for (let index = 0; index < delArry.length; index++) {
          if (delArry[index].id === delCommentDataMsg.commentData.data.id) {
            delIndex = index;
          }
        }
        state.allComments[delCommentDataMsg.ComIndex].childrenlist.splice(
          delIndex,
          1
        );
      }
    },
  },
  getters: {
    getAllComments(state) {
      // console.log("@@", state);
      return state.allComments;
    },
  },
};
