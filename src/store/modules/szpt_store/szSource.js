import _ from "lodash";
export default {
  namespaced: true,
  state: {
    postDataByID: {},
    postMsg: null,
  },
  actions: {
    init_SOURCEDATA(contex, postDataByID) {
      contex.commit("INIT_SOURCDATA", postDataByID);
    },
  },
  mutations: {
    INIT_SOURCDATA(state, postDataByID) {
      // 初始化资源组件数据,浅拷贝
      state.postDataByID = postDataByID;
      state.postMsg = postDataByID;
      // console.log("mutation开始了....", postDataByID, state.postDataByID);
    },
  },
  getters: {
    getNewsPostData(state) {
      // 获取最新的postData
      // console.log("getter打印", state.postDataByID, state.postMsg);
      // console.log("getters打印一下", state);

      return state.postMsg;
    },
  },
};
