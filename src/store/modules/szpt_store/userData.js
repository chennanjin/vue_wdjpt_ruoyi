export default {
  namespaced: true,
  state: {
    userDataMsg: {},
  },
  actions: {
    init_userDataMsg(contex, userDataMsg) {
      contex.commit("INIT_USERDATAMSG", userDataMsg);
    },
  },
  mutations: {
    INIT_USERDATAMSG(state, userDataMsg) {
      state.userDataMsg = userDataMsg;
      // console.log("userData", state.userDataMsg);
    },
  },
  getters: {
    // 返回用户个人基本信息
    getUserDataMsg(state) {
      // console.log("state", state.userDataMsg);
      return state.userDataMsg;
    },
  },
};
