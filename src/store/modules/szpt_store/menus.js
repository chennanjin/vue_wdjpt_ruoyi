// 菜单状态管理模块
export default {
  namespaced: true,
  state: {
    // 菜单初始化数组
    menusRoutes: [],
    // 标签栏初始化数组
    tagList: [],
    // 子列表标签栏初始化数组
    childrenlists: [],
  },
  actions: {
    getNavigationroutes(contex) {},
  },
  mutations: {
    INIT_MENUS(state, value) {
      // 菜单路由初始化
      state.menusRoutes = value;
      // 将获取的路由导航存到本地
    },
    INIT_TAGLIST(state, value) {
      // 标签栏初始化
      state.tagList = value;
    },
    INIT_CHILDRENLISTS(state, value) {
      state.childrenlists = value;
    },
    INIT_NAVIGATION_ROUTES(state, value) {
      state.NavigationRoutes = value;
    },
  },
};
