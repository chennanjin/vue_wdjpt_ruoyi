// 这是一个格式化tagList数组转化成级联数据的函数
export function fmtOptionData(DataArry) {
  //   定义一个接收参数的数组
  if (DataArry instanceof Array) {
    let fmtArry = [];
    if (DataArry) {
      DataArry.forEach((list) => {
        let fmtData = {
          value: list.id,
          label: list.name,
        };
        if (list.childrenlist) {
          let ArrayList = [...fmtOptionData(list.childrenlist)];
          if (ArrayList.length) {
            // 当数组对象存在时对其进行赋值，否则不赋值
            fmtData.children = ArrayList;
          }
        }
        fmtArry.push(fmtData);
      });
    } else {
      console.log("数组不存在或数组错误！");
    }
    return fmtArry;
  }
}
// 格式化需要发送的信息格式
export function fmtTagDto(dataMsg, navData) {
  // 获取导航栏名称
  if (navData instanceof Array) {
    navData.forEach((navItem) => {
      if (dataMsg.NavID === navItem.id) {
        dataMsg.NavName = navItem.remark;
      }
    });
  }
  let postData = {
    tagDTOList: [
      // 可以发布在多个模块下，根据id区分
      {
        data: {
          id: dataMsg.NavID,
          name: dataMsg.NavName,
        },
      },
    ],
    data: {
      // 帖子标题
      title: dataMsg.title,
      // 富文本框内容
      content: JSON.stringify(dataMsg.content),
      // 描述
      remark: dataMsg.remark,
      type: "POST",
      score: null,
    },
  };
  return postData;
}
